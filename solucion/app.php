<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Este obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
<?php
echo '<br></br>';
error_reporting(0);
require(__DIR__.'/lib/HTTPClient.php');
require(__DIR__.'/lib/JSONParser.php');

####################################################
####################################################
###########  EXEMPLE client HTTP ###################
####################################################
####################################################

# Primer instanciar la classe, amb la base URL on hi ha la nostre api
$base_url   = 'http://127.0.0.1:3000';
$token      = 'C0UsWlYxXrMx81TKN2Eq'; //taken = v1.js per identificar davant de l'api
$client     = new HTTPClient($base_url, $token);

## Mitjançant el client instanciat anteriorment, podem realitzar crides a l'API amb varis metodes
## i també enviar parametres.
//$client->query(string $uri, array $params = [], string $method='GET')
# Aquesta petició sempre ens retornarà un array amb dos camps:
## status =  true / false 
## data = retorn de l'API
/*
$result = $client->query('endpoint');
$result = $client->query('endpoint', $params, 'POST');
$result = $client->query('endpoint', $params, 'PUT');
$result = $client->query('endpoint', [], 'DELETE');
*/


####################################################
####################################################
###########  EXEMPLE parser JSON ###################
####################################################
####################################################
//PART 1
//COMO IMPORTAR ALUMNOS (de alumnes.json) Y ASSIGNATURAS
//NOSALTRES ESPECIFIQUEM DADES DE JSON Y LES TRANSFORMA EN ARXIU DE PHP


## status =  true / false 
## data = array amb els valors del json o el codi d'error
# Si necessitem enviar paramatres a l'API, primer crearem un array amb els values

//IMPORTAR DADES DELS ALUMNES

$result = JSONParser::parseFile(__DIR__.'/alumnes.json');//buscar directory
var_dump($result['data']);
foreach ($result['data'] as $alumne){
    $alumne = get_object_vars($alumne);
    $params = [ 
            "nom" => $alumne["nom"],  
            "cognoms" => $alumne["cognoms"],   
            "mail" => $alumne["mail"]
    ]; //li pasem totes les variables i l'api seleccionarà els camps que entrarà a la bbdd
    $result=$client->query('/api/v1/alumne', $params, 'POST');
}
die();

echo '<br></br>';
//IMPORTAR DADES DE LES ASSIGNATURES
$result = JSONParser::parseFile(__DIR__.'/assignatures.json');//buscar directory
var_dump($result['data']);

foreach ($result['data'] as $assignatura){
    $assignatura = get_object_vars($assignatura);
    $params = [ 
            "nom" =>  $assignatura["nom"],  
            "professor" =>  $assignatura["professor"]
    ]; //mostrar resultat
    $result=$client->query('/api/v1/assignatura', $params, 'POST');
}
die();

//VINCULAR ALUMNES I ASSIGNATURES
echo '<br></br>';
$count = 0;
$result = $client->query('/api/v1/alumnes');
while($result["data"][$count] != null)
{
    $params = [
        'alumne_id' => $result["data"][$count]->id,
        'assignatura_id' => 1
    ];
    var_dump($params);
    $insertar = $client->query('/api/v1/vincular', $params, 'POST');
    $count++;
    $result = $client->query('/api/v1/alumnes');
}
echo "<br/>";
$count = 0;
while($result["data"][$count] != null)
{
    $params = [
        'alumne_id' => $result["data"][$count]->id,
        'assignatura_id' => 2
    ];
    var_dump($params);
    $insertar = $client->query('/api/v1/vincular', $params, 'POST');
    $count++;
    $result = $client->query('/api/v1/alumnes'); 
} 
//INSERTAR ALUMNES ASSIGNATURES RELACIO NOTES

$result = JSONParser::parseFile(__DIR__.'/alumnes.json');//buscar directory
var_dump($result['data']);
foreach ($result['data'] as $alumne){
    $alumne = get_object_vars($alumne);
    $params = [ 
            "alumne_id" => $alumne_nota,  
            "assignatura_id" => $assignatura_nota
    ]; //li pasem totes les variables i l'api seleccionarà els camps que entrarà a la bbdd
    $result=$client->query('/api/v1/nota', $params, 'POST');
}
die();
/*

//FUNCIONS QUE CRIDAR

public function SeleccionarAlumne($id)
{
    $params = [ 
        "id" => $id
    ];
$result=$client->query('/api/v1/alumne/:'.$id, $params, 'GET');
return $result["data"];
}

public function ActualitzarAlumne($id,$nom,$mail)
{
    $params = [ 
        "id"  => $id,
        "nom"  => $nom,
        "mail"  => $mail
    ];
    $result=$client->query('/api/v1/alumne/:'.$id, $params, 'PUT');
    return $result["data"];
}

public function EliminarAlumne($id)
{
   $id=$_GET['id'];

$result=$client->query('/api/v1/alumne/:'.$id, [], 'DELETE');
die();
}

public function SeleccionarAssignatura($id)
{
    $params = [ 
        "id" => $id
    ];
$result=$client->query('/api/v1/alumne/:'.$id, $params, 'GET');
return $result["data"];
}
public function ActualitzarAssignatura($id,$nom,$professor)
{
    $params = [ 
        "id"  => $id,
        "nom"  => $nom,
        "professor"  => $professor
    ];
    $result=$client->query('/api/v1/alumne/:'.$id, $params, 'PUT');
    return $result["data"];
}

public function EliminarAssignatura($id)
{
   $id=$_GET['id'];
   $result=$client->query('/api/v1/assignatura/:'.$id, [], 'DELETE');
   die();
}
?>

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Este obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
*/